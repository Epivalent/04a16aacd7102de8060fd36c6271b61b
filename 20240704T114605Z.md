To create an optional match group in Perl regex, you can use the question mark `?` quantifier after the group. Here's an example:

```perl
my $string = "Hello World";
if ($string =~ /(Hello)? World/) {
    print "Matched: $1
" if defined $1;
}
```

In this example, the `(Hello)?` group is optional because of the `?` quantifier. It will match zero or one occurrence of "Hello". The output will be "Matched: Hello" as it matches the optional group.

If you remove the word "Hello" from the string, it will still match because the group is optional:

```perl
my $string = "World";
if ($string =~ /(Hello)? World/) {
    print "Matched: $1
" if defined $1;
}
```

The output will be empty as there is no match for the optional group.
# Thu  4 Jul 11:46:05 CEST 2024 - perl regex syntax for an optional match group?